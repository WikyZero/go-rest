package main

import (
	"net/http"
	"log"
)

func main(){

	router := NewRouter()

	server := http.ListenAndServe(":8080", router)
	log.Fatal(server)
}

// router https://github.com/gorilla/mux
	//crear una ruta SIN MUX
	//http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request){
	//		fmt.Fprintf(w, "Primera respuesta")
	//	})