package main

import (
	"net/http"
	"github.com/gorilla/mux"
)

type Route struct{
	Name string
	Method string
	Pattern string
	HandleFunc http.HandlerFunc
}

type Routes[]Route

func NewRouter() *mux.Router{
	router := mux.NewRouter().StrictSlash(true)

	for _, route := range routes{
		router.Methods(route.Method).Path(route.Pattern).Name(route.Pattern).Handler(route.HandleFunc)
	}

	return router
}

var routes = Routes{
	Route{"Index", "GET", "/", Index},
	Route{"VehicleList", "GET", "/vehicles", VehicleList},
	Route{"VehicleShow", "GET", "/vehicle/{id}", VehicleShow},
	Route{"VehicleAdd", "POST", "/vehicle", VehicleAdd},
	Route{"VehicleUpdate", "PUT", "/vehicle/{id}", VehicleUpdate},
	Route{"VehicleRemove", "DELETE", "/vehicle/{id}", VehicleRemove},
}