package main

import (
	"fmt"
	"net/http"
	"github.com/gorilla/mux"
	"encoding/json"
	"gopkg.in/mgo.v2"
	"log"
	"gopkg.in/mgo.v2/bson"
)

func GetSession() *mgo.Session{
	session, err := mgo.Dial("mongodb://localhost")

	if(err != nil){
		panic(err)
	}

	return session
}

type Message struct{
	Status string `json:"status"`
	Message string `json:"message"`
}

func (this *Message) setStatus(data string){
	this.Status = data;
}

func (this *Message) setMessage(data string){
	this.Message = data;
}


var collection = GetSession().DB("wikyzero").C("vehicles")


// var vehicles = Vehicles{
// 				Vehicle{"Seat", "Ibiza", 90},
// 				Vehicle{"Fiat", "Panda", 45},
// 				Vehicle{"Subaru", "Impreza", 240},
// 			} 

func Index(w http.ResponseWriter, r *http.Request){
	fmt.Fprintf(w, "Primera respuesta")
}


func VehicleList(w http.ResponseWriter, r *http.Request){

	var results []Vehicle
	err := collection.Find(nil).All(&results)

	if(err != nil){
		log.Fatal(err)
	}else{
		fmt.Println("resultados: ", results)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(results);
}


func VehicleShow(w http.ResponseWriter, r *http.Request){
	params := mux.Vars(r)
	vehicle_id := params["id"]

	if !bson.IsObjectIdHex(vehicle_id){
		w.WriteHeader(404)
		return
	}

	oid := bson.ObjectIdHex(vehicle_id)

	results := Vehicle{}
	err := collection.FindId(oid).One(&results)

	if(err != nil){
		w.WriteHeader(404)
		return
	}else{
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		json.NewEncoder(w).Encode(results);
	}
}

func VehicleAdd(w http.ResponseWriter, r *http.Request){
	decoder := json.NewDecoder(r.Body)

	var vehicle_data Vehicle
	err := decoder.Decode(&vehicle_data)

	if(err != nil){
		panic(err)
	}

	//Cerrar lectura del body
	defer r.Body.Close()

	err = collection.Insert(vehicle_data)

	if(err != nil){
		w.WriteHeader(500)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(vehicle_data)
	//vehicles = append(vehicles, vehicle_data)

}


func VehicleUpdate(w http.ResponseWriter, r *http.Request){
	params := mux.Vars(r)
	vehicle_id := params["id"]

	if !bson.IsObjectIdHex(vehicle_id){
		w.WriteHeader(404)
		return
	}

	oid := bson.ObjectIdHex(vehicle_id)

	decoder := json.NewDecoder(r.Body)
	
	var vehicle_data Vehicle
	err := decoder.Decode(&vehicle_data)

	if err != nil {
		panic(err)
		w.WriteHeader(500)
		return
	} 

	defer r.Body.Close()

	// results := Vehicle{}
	// err = collection.FindId(oid).One(&results)

	document := bson.M{"_id": oid}
	change := bson.M{"$set": vehicle_data}
	err = collection.Update(document, change)

	if(err != nil){
		w.WriteHeader(404)
		return
	}else{
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		json.NewEncoder(w).Encode(vehicle_data);
	}
}

func VehicleRemove(w http.ResponseWriter, r *http.Request){
	params := mux.Vars(r)
	vehicle_id := params["id"]

	if !bson.IsObjectIdHex(vehicle_id){
		w.WriteHeader(404)
		return
	}

	oid := bson.ObjectIdHex(vehicle_id)

	err := collection.RemoveId(oid)

	if(err != nil){
		w.WriteHeader(404)
		return
	}
	message := new(Message)
	message.setStatus("success")
	message.setMessage("Vehicle with ID: "+vehicle_id+" has been deleted")


	results := message
	w.Header().Set("Content-Type","application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(results)
}


