package main


type Vehicle struct {
	Marca string 	`json:"marca"`
	Modelo string	`json:"modelo"`
	Cv float32		`json:"cv"`	
}

type Vehicles []Vehicle